# Term project SRS 2019

## Team information
* Ari Ernesto 
  * Mail: t107999401@ntut.org.tw		
  * ID: 107999401
* Thomas Riedler 
  * Mail: t108998403@ntut.org.tw		
  * ID: 108998403

## Problem statement

We are considering to be the company who build the lobby display for the old lift. Now we have enough knowledge to build a simple lift controller for a small hotel. The hotel has 4 floors (1 = Lobby, 2 - 4 Rooms).

There are safety features like locking mechanism and safety switch at the doors, which are very important and need to have priority over all other commands.

Through having a complete system, we know now always where the lift is positioned and in which direction it is traveling. But we need to check, if the cabin is also moving if we apply the corresponding signals → otherwise we need to give error signals to the staff at the reception.

There is one button for request per floor and one buttons for every floor inside the cabin. This

The boss of the hotel wants that the lift, if not serving people for 10 minutes to wait in the lobby. This is due to newly arrived customers shouldn’t wait for the lift.

**Actuators:**
* The doors of the cabin
* The doors of the actual floor
* The motor for raise and lower the cabin
* The locking mechanism when the lift is serving a floor (safety)

**Sensors:**
* Request button on floors
* Floor selection button in the cabin
* Encoder of the motor (gives an analogue actual position)
* Sensor for arriving at floor
* Door-safety-Sensor (when something blocks the door)
* Sensor for every door open and close
* Weight sensor

**Displays:**
* In every floor is a display, which indicates in which floor the cabin is, which direction moves and where are outstanding requests. All four Displays show the same information.
* There is a display in re reception, which is showing errors in case the lift is stuck in-between, or somebody occupy it for too long on a floor.


We consider all models and information within the machine to be volatile. This means when there is even a long power failure, we do not need to make all inputs again and the lift needs not a kind of reference run.

  ### General Parameters:

*    Height of floors: 350cm
*    Speed of lift: 50cm/s (due to temperature changes 45 - 55cm/s is possible)
*    Time opening it takes doors opening / closing completely: 3s
*    Doors staying fully open for serving a floor: 7s
*    The doors stay fully open after reopening cause of safety switch: 1s
*    the lift departures after door fully closed: 0.8s
*    Encoder gives 1 impulse per 0.5cm of moving in corresponding direction.
*    Doors re-open to 5 times when safety switches of doors
*    After power up the reset button needs to be pressed to ensure that the lift is in ok condition
*    If the lift is not at a floor after power up, the lift is going to the next floor and opens the doors there to let passengers exit the cabin.

## Decomposition into sub-problems

### Context Diagram:
<img src="https://i.imgur.com/sOE5Mzt.jpg" height="95%" width="95%"/>

#### Lift Controlling Machine: [C]
This is the machine which is controlling every domain shown in the context diagram. It is assumed that the machine is a single unit and not distributed to different peripheral units. The connection between the single domains and the central unit is seen as reliable.

#### Elevating Motor:  [C]
The main motor is a very important part. Without turning of the motor, no movement is possible. The motor is driving through a reduction gearbox the winch for pulling the rope, which holds the cabin. The gearbox is assumed to be self-locking, so no additional break would be needed. Due to safety reasons a locking mechanism is installed, which is controlled separately. To simplify the controlling a bit, we assume to have just signals for turning the motor on, off and give the direction UP and DOWN.

**Input:** `O_ON, O_UP, O_DOWN, O_OFF`

**Output:** `M_turning_UP, M_turning_DOWN`

#### Motor Encoder:  [C]
The Encoder is seen as own domain, although placed directly on the main motor. The encoder is transferring the phenomena of turning motor into impulses, which we count for finding out the actual position.

**Input:** `M_turning_UP, M_turning_DOWN`

**Output:** `S_Imp_UP, S_Imp_DOWN`

#### Cabin Emergency Button:  [C]
In case of an emergency the lift user can call for help when pressing this button long enough.

**Output:** `B_Em`

#### Emergency Number:  [C]
In case of pressing the emergency button long enough, a predefined number gets called and also a message with lift ID and actual floor is send to this number. This is for example the local center of the lift manufacturer or the reception staff.

**Input:** `O_Alarm, Floor f, Lift ID`

#### Request Button Floor(n):  [C]
On every floor there is a single request button. If this button gets pressed, the controlling machine will store this request and serve this floor. There are two ways how to serve:
1. The lift is already in movement: if passing a floor with outstanding request, we will serve this floor.
2. The lift is not moving: the lift will start moving towards the floor with outstanding request.

**Output:** `B_Requ(n)`

#### Floor(n) selection button in cabin:  [C]
In the cabin, there is a button to select the floor, the user wants to go. For a single user it is easy to determine where to go. If there are multiple users, wanting to different floors, following logic is applied:
* It is the same logic for a request from the floor to call the cabin and from inside the cabin to go by the cabin to a specific floor
* First destination keeps the highest priority in the dynamic queue. If the first user entered in the wants to go up, the lift will go up.
* If there is a new destination between actual position and the position we are heading, we will serve this to save time.
* A floor is served, if the cabin has stopped at that floor and the doors of floor and cabin are both opened successfully.

**Output:** `B_Floor(n)`

#### Display at Floor(n) and in Cabin  [C]
The Display at each floor looks like shown. It is there to give the user at the floor a good overview, where the lift is currently located and where it will stop. The arrows on the left side are indicating in which direction the lift is moving. The circles in the middle show at which floor the lift is currently located. The stars to the right side show a floor with outstanding request.
The display is the same on every floor and the lights are also showing everywhere the same. So, we do not need to bother about showing at every floor different state, every display gets same commands and displays the same information.

**Input:** `L_UP_ON, L_UP_OFF, L_DOWN_ON, L_DOWN_OFF, L_Floor(n)_ON, L_Floor(n)_OFF, L_Requ(n)_ON, L_Requ(n)_OFF`

<img src="/uploads/-/system/personal_snippet/1925960/84f97eea344a9bfa7d95c28355871c83/Display_Floors.JPG" width="300"/>

#### Lift User:  [B]
The lift user is a person, which is using the service of our lift. The user can input through the request button and floor selection button with our machine. The machine gives feedback to the user by the displays at the floors and in the cabin.
The target for the user is to come as fast as possible from his actual floor to a desired floor.
The user requests can be separated in two sub-requests:
* The user requests the cabin to come to his actual floor by pressing the request button. When the Cabin arrives at this floor and has opened the doors fully, this request is fulfilled.
* In normal case the user will then enter the cabin and give the machine the second input: The floor he wants to go to b pressing the button inside the cabin. In this case the doors will close again, and the cabin moves to the corresponding floor. When the cabin arrived at this floor and the doors are fully opened, this request is fulfilled.


#### Reception Display:  [C]
The Display in the repetition is showing the basic errors, to give the hotel staff a message if something is not okay. The reason is that the staff can try to fix the issue: For the five error messages following scenarios are possible to fix them:
* Cabin is stuck: 
  *	Action of Staff: Check if persons are stuck in the cabin.
  *	Error solving: Call maintenance staff
* Maximum Weight exceeded:
  *	Action of Staff: Go to corresponding floor
  *	Error solving: Reduce weight in cabin
* Doors Stuck:
  *	Action of Staff: Go to corresponding floor
  *	Error solving: Check if obstacle is in the guide of the doors, Reset Error after removed obstacle
* Doors are blocked:
   *	Action of Staff: Go to corresponding floor
   *	Error solving: Check if obstacle is in doors, Reset Error after removed obstacle
* Power Failure:
   *	Action of Staff: Go to corresponding floor and check if lift is empty and in safe condition
   *	Error solving: Press reset button to bring lift in normal operation again

**Input:** `L_UP_ON, L_UP_OFF, L_DOWN_ON, L_DOWN_OFF, L_Floor(n)_ON, L_Floor(n)_OFF, L_Requ(n)_ON, L_Requ(n)_OFF, L_E(x)_ON, L_E(x)_OFF`

<img src="/uploads/-/system/personal_snippet/1925960/d5f1219a23b5a12d4d52225430dae450/Display_Reception.JPG" width="600"/>

#### Reception Staff:  [B]
The reception staff gets informed over the reception display about the current lift status. The possible errors are shown by square lamps and allow the staff to maybe fix the error by themselves or call for further help. Possible scenarios shown in before description.
The only input the staff can make, is pressing the error reset button. By pressing this button, the lift is starting normal operation again after an error happened. There are no more things the staff can press in order to fix problem with the lift. This is due to the staff is expected to change frequently and do not be educated in a technical way.

#### Error Reset:  [C]
If there was an error or the power was gone, the lift needs to be reset by the reception staff. By pressing the button, the main state machines is going back in initial state and start with normal operation again.
Typical scenarios for pressing this button is, when there was something blocking the doors and the staff removed it. Due to the lift stopped after five times trying to close the doors again, there needs to be an input for returning to standard operation.

**Output:** B_Reset

#### Sensor at Floor(n):  [C]
In every floor there is a Sensor that gets triggered from the cabin, when being +/- 5cm within the position to open the doors. The Sensors are very important to get the exact position from the cabin, regardless about the reading of the encoder on the motor. Every floor has a height of 250cm, so the sensor gets triggered like shown in picture below.

<img src="/uploads/-/system/personal_snippet/1925960/27acc1d57f6b239cd4949c2b5ae673c7/Dimensions.jpg" zoom="50%"/>

The sensors are very important for the procedure of opening the doors of a specific floor, because we need to start breaking when the sensor turns on of a floor we serve.
This leads to the fact, that we only obey a request before the switch of the corresponding floor is on. This prevents too fast braking maneuvers which cause additional wear in the mechanic.
Due to the fact that the sensors give us always the exact position of the cabin, there is a logic to match the value of the encoder always when passing a floor sensor and the deviation is too large between the value calculated by the impulses of the encoder and the real value.

Causes for these deviations can be following: 
* Different Temperature and so different length of the rope
* Different loading of the cabin
* The controlling machine lost some impulses of the encoder
* The cables increase their length due to wear.

**Output:** `S_ON_Floor(n)`

#### Safety Sensor Door Floor(n):  [C]
Every Door at the floors (the outside doors) is equipped with a safety sensor, that is triggered when some obstacle or person is in the doors when closing them. The result of a triggered switch during closing the doors, is a reopening of both doors (cabin and corresponding floor). The doors keep 1 second fully opened after triggering a safety switch. After five times reopening due to a safety sensor, the doors keep open and the lift is stopping its operation.

**Output:** `S_D_S_F (n)`

#### Safety Sensor Door Cabin:  [C]
The door of the cabin is also equipped with a safety sensor, leading to the same result than the sensor of the floor doors. With the sensor of the cabin door it is important to just open the floor doors of the floor we are currently.

**Output:** `S_D_S_C`

#### Sensor Door opened/shut Cabin:  [C]
The doors of the cabin could eventually be blocked or not moving due to some mechanical failure. Not opening door would have the result that the persons cannot leave the cabin and keep caught in the cabin. For this case there is the emergency button in the cabin to call for help. Second possible problem is, when the doors do not close. This do not sound dangerous at first but imagining that the cabin is moving without closed door, this could lead to severe injuries. So, the lift is not allowed to move again, before the cabin doors are not closed.
This leads to following two scenarios:
* Doors not opening we try to open again, when not successful at second attempt → go in error state and stop lift
* Doors not closing: → go in error state and stop lift. It is more likely that the doors are not closing due to some obstacles blocking the mechanic. In this case the staff can remove this obstacle, press reset button and the lift will start normal operation again.

**Output:** `S_D_C_opened, S_D_C_shut`

#### Sensor Door opened/shut Floor(n):  [C]
Also, the doors of the floors have sensors to check if they are neither opened nor shut. The problem with not opening doors is the same as for the cabin: persons cannot leave the cabin.
For doors not shut the possible injuries are even worse: a person could fall into the lift shaft or could get caught between cabin and door frame. Both scenarios need to be avoided. So, when the floor doors are not shut, the lift is also not allowed to move.
The scenarios for not receiving the feedback sensor after sending the door command, is the same as for the cabin doors.

**Output:** `S_D_F(n)_opened, S_D_F(n)_shut`

#### Weight Sensor:  [C]
At the motor and winch unit there is a sensor, that is monitoring the weight of the whole components (cables, cabin, motor, gearbox, winch). By setting the sensor after installation at empty cabin, the payload (W) can be calculated by the sensor and send constantly to the machine. The nominal payload of the installed lift is 500kg. If exceeding 550kg (+10%), an alarm will be triggered in the cabin. This should force the users to decrease the weight. If the weight is below 450kg (-10%) the machine turns off the alarm again.
It is important to only check the weight in case of stopped cabin. During moving upwards, the forces will be much higher due to friction and inertia force while accelerating. The ±10% of hysteresis are to minimize the risk of false detections caused by jumping or hopping in users.
If the weight is exceeded during serving a floor, it will cause the doors to not close until the weight is reduced.

**Output:** `S_Weight(W)`

#### Alarm overloaded:  [C]
If there is overweight detected during serving a floor by the weight sensor, there needs to be issued an alarm, in order to let the passenger(s) know what the problem is. Otherwise they would not know, why the doors of the cabin are not closing, and the lift stays at the floor.

**Input:** `O_Alarm_ON, O_Alarm_OFF`

#### Locking Mechanism:  [C]
The locking mechanism is a security feature that will allow the cabin only to move if there is power and signal from the controlling machine. Without locking, the doors are not allowed to be opened. After serving a door and closing the doors, the locking needs to be switched off again to allow the cabin to move again.

**Input:** `O_Lock_ON, O_Lock_OFF`

#### Cabin Doors:  [C]
The doors of the cabin are just allowed to open when the cabin is within the range of a floor sensor and the locking mechanism is on. When receiving a `O_D_C_OPEN` signal the doors will fully open. In case of normal operation this will take three seconds. After receiving a `O_D_C_CLOSE` signal, the doors will normally close within three seconds fully. The maximum time for closing / open is set to 5 seconds. This means for example when issuing the command `O_D_C_OPEN` by the machine and not receiving a `S_D_C_opened` within 5 seconds there is a problem with opening. Same logic for closing the doors.

**Input:** `O_D_C_OPEN, O_D_C_CLOSE`

#### Doors Floor(n):  [C]
The doors of the floor #n are just allowed to open when the cabin is within the range of `floor(n)` sensor and the locking mechanism is on. The doors of cabin and the floor shall work synchronously, so the times and errors are the same for both doors.

**Input:** `O_D_F(n)_OPEN, O_D_F(n)_CLOSE`


## Analysis 
The Lift as real world is too complex to just give simple inputs like `GO`, `STOP` and we get the desired reaction. 
The same comes true, when we want to show the actual state, we have some constant input like the position, but without a model it is not possible to show all current states correctly.

### Model Building Subproblem:
The model of the whole lift (Motor, Lock, Doors, Sensors):

<img src="/uploads/-/system/personal_snippet/1925960/eb1b48ed409430cfbb3ef355db29c2cd/Model_Building.jpg" width="1000"/>

To make the model easier, the model is divided into following sub models:
* `Position(p)`: Here the actual position is calculated, and additional calculations are done with the position
* `OSR_Queue`: For the outstanding requests we need a model for the queue, to proper process all requests
* `Serve(k)`: The general state machine for serving floors and moving between the floors

#### Model of actual position “Position(p)”:

<img src="/uploads/-/system/personal_snippet/1925960/7601f1628da09c7bb44562edbc387e86/Model_Position.jpg" zoom="30%"/>

So where are transferring the impulses from the encoder into an actual position in centimeters. For the displays and the decision in which direction the lift needs to go next, the centimeter value is transferred into a floor f information.
When it comes to restarting the lift after a power failure, it is necessary to know which floor is the nearest to the actual one, this gets calculated and stored in the variable `nf`.

#### Queue Model for Outstanding requests, “OSR_Queue”:
The OSR_Queue model is made for simpler handling with outstanding requests. The queue itself is at FIFO principle → the first request keeps always first at the queue. So all request we add, will be added at the end of the queue. When the lift is starting from a certain floor, the destination is the OSR first in the queue. But when we pass a floor, where there is an outstanding request, we serve this floor. In this case, all OSR for this floor will be deleted.

<u>**Output:**</u>
* `OSR`: There are outstanding requests → otherwise the lift can stop
Example: || → `OSR` = false, |1|3| → `OSR` = true
* `OSR_F(n)`: There is an outstanding request for floor n
Example: |3| → `OSR_Floor(2)` = false, `OSR_Floor(3)` = true 
* `First_OSR`: The first of the Requests in the queue is floor x 
Example: |1|4| → `First_OSR` = 1

<u>**Input:**</u>
*  `Add_R(n)`: Add request to the queue for floor number n
Example: || → `Add_R(3)` → |3|, |2| → `Add_R(4)` → |2|4|
*  `Remove_R(n)`: Remove requests for floor number n
Example: |1|2|3|4| → `Remove_R(2)` → |1|3|4| 
*  `Remove_All`: Remove all requests from the Queue

<u>**Example:**</u>
* The lift is at floor #3, the queue looks like following: |1|4|. 
* So, we have outstanding requests (`OSR` = true) and the first OSR is Floor number 1 (`First_OSR` = 1). The lift will head to floor number 1. 
* Before reaching floor number 2, there somebody press the request Button on floor number 2 → `Add_R(2)` → queue |1|4|2|.
* When we reach floor number 2, `OSR_F(2)` = true. 
* The lift will Stop at floor 2 and serve it. 
* So the OSR for floor 2 will be deleted (`Remove_R(2)`). → queue |1|4|. 
* The lift will go on to go towards the OSR first in queue (`First_OSR` = 1). 
* The lift will arrive floor 1 and serve it (`OSR_F(1)` = true). 
* After served the floor, the queue is updated (`Remove_R(1)` . → queue |4|. 
* Now the first element is floor number 4 (`First_OSR` = 4) and the lift Move upwards.

#### State Machine for serving a floor “Serve(k)”

<img src="/uploads/-/system/personal_snippet/1925960/60aa3f027a56541e46ff7ecfb6c519bc/Model_Serve.jpg" zoom="25%"/>

The state machine is dialing with waiting for outstanding requests, deciding if going upwards or downwards and if to serve a passing floor or not.
When really serving a floor, it is important to reopen the doors in case a safety sensor of the doors has triggered.
If an error occurs, the state machine is set to state #0 and needs the OK signal to can start with state #1 again.

#### Output Commands from the Model Machine:
<img src="/uploads/-/system/personal_snippet/1925960/cb9689c0b79d3aa61be33b91fc247c92/Table_Signal_IN.JPG" width="800"/>


#### Output Commands from the Model Machine:
<img src="/uploads/-/system/personal_snippet/1925960/48fa1527eac274b81fdf1675f226b758/Table_Signal_OUT.JPG" width="800"/>

### Riding to Floor(t), Commanded behavior:
Serving user requests to call the cabin to a floor, or go with the cabin to another floor:

<img src="/uploads/-/system/personal_snippet/1925960/2ee8c66a784ff074151816c3caaf04e5/Model_Serving_Floor.jpg" zoom="80%"/>

The riding to a floor by user input can have two different triggers:

1. `B_Floor(t)`: A User is in the cabin and presses the button to get transported by the cabin to a specific floor
2.	`B_Request(t)`: A User is at a floor and presses the request button there, in order to make the cabin move to that floor.
Both Inputs are sensed by the lift controlling machine. Here is every input sensible, because there is only one button for each function.
The Lift controlling machine then is checking if there is already an outstanding request for the floor the User want the cabin to move to. If `OSR_F(x)` = False, the machine is adding the floor to the OSR_Queue by command Add_R(t).
Depending on the actual state of the lift model, the `serving(k)` state machine will process this request. By processing this request, commands are issued by the lift model and the model machine itself senses this and issue commands to the real-world components like motor, lock, doors.

#### Frame Concern:
<img src="/uploads/-/system/personal_snippet/1925960/5e3832097faa031bfa7fdbbbdf560149/Frame_Concern_1.PNG" zoom="67%"/>

The lift is on floor #4 with closed doors the lift user inside the cabin is pressing the button to go to floor #2. There is no other request for floor #2.

1.	Lift user is issuing command `B_Floor(2)`
2.	The Lift controlling machine is sensing this `B_Floor(2)`
3.	The lift controlling machine is issuing the command `Add_R(2)` because there is currently no request for Floor #2.
4.	The lift model “serve(k)” changes state from 1 – 2 – 3 – 5 ,……. And issues commands `M_Down`, `M_Lock_Off`,..
5.	The model machine issues commands corresponding to the inputs,  `O_ON`, `O_DOWN`, `O_Lock_OFF`,..
6.	The model machine detects the commands `M_Down`, `M_Lock_OFF,`..
7.	The model machine issues commands corresponding to the inputs,  `O_ON`, `O_DOWN`, `O_Lock_OFF`,..
8.	The lift switches off the lock, moves down,….
9.	Which leads to the lift coming to the second floor.

### Go to floor after 10min no user request, required behavior:
<img src="/uploads/-/system/personal_snippet/1925960/7e3ec35d3b48875ffee6856bcc0054e5/Model_10min_Lobby.jpg" zoom="67%"/>

The hotel boss wants the cabin always to wait in the lobby. So, there is a required behavior problem frame needed to fulfill this requirement.
The lift controlling machine is observing the state of `serving(k)` and the actual floor number. When state #1 is continuously active for 10 minutes and the cabin is not already on floor #1, the machine is adding a request for floor #1 to the queue. This makes the lift model to process this request and the cabin will ride to floor #1.

#### Frame Concern:
<img src="/uploads/-/system/personal_snippet/1925960/4243cd93d2060956f32aec48ba19ff0d/Frame_Concern_3.PNG" zoom="67%"/>

The lift is on floor #2 with closed doors and no request for 10 minutes
1.	Serve(k)=10min & ¬ f=1
2.	The lift controlling machine is issuing the command Add_R(1) to the lift model
3.	The lift changes state from 1 – 2 – 3 – 5 ,……. And issues commands `M_Down`, `M_Lock_Off`,..
4.	The model machine detects the commands `M_Down`, `M_Lock_OFF`,..
5.	The model machine issues commands corresponding to the inputs,  `O_ON,` `O_DOWN`, `O_Lock_OFF`,..
6.	The lift switches off the lock, moves down,….
7.	Which leads to the lift coming to the first floor (lobby) 

### Reception Display, information display:
<img src="/uploads/-/system/personal_snippet/1925960/3744da96a1e135766c99860839994745/Model_Reception_Display.jpg" zoom="67%"/>

The display in the reception will be explained here in detail. The display on the floors is not explained here, because it is mainly the same display, just the error indication lamps are missing.
The lift controlling machine is here transferring specific information from the lift model to be able to switch on and off the specific lamps. This will be done with a continuously running loop, that is checking the states of the lift model. The loop is not shown here, just which input is leading to which output:
<img src="/uploads/-/system/personal_snippet/1925960/ef6124152208f9c5012c56b7cb8df31e/Table_Display_Signals.JPG" zoom="50%"/>


#### Frame Concern:
<img src="/uploads/-/system/personal_snippet/1925960/42b6bbc6080fcae1d6f2d909b570d4e0/Frame_Concern_2.PNG" zoom="67%"/>

The lift is stopped on floor #1 with closed doors and a user presses the request button on floor #4. There is no other request currently.
1.	Lift user pressing the request button on floor #4
2.	Due to the properties of the Lift & User Domain the command `B_Requ(4)` is issued 
3.	The model machine detects this command.
4.	And the model machine is issuing command `Add_R(4)` because there is currently no outstanding request for this floor.
5.	The lift model `serve(k)` changes state from 1 – 2 – 3 – 4 ,……. And issues commands `M_UP`,.. And the queue model looks like following: |4|
6.	The lift controlling machine detects the commands `M_UP` and that `OSR_F(4)` is True now
7.	The lift controlling machine is issuing corresponding to the inputs the outputs `L_UP_ON` and `L_Requ(4)_ON`
8.	The reception display turns on accordingly to the inputs the up-arrow and the request start
9. The reception staff can see that the up-arrow and the request star for floor #4 are lit, so that the lift is going up and there is a request on floor #4.

### Calling Emergency Number, Commanded behavior: 
<img src="/uploads/-/system/personal_snippet/1925960/ef3485cc3048f7fcf1467de0c730d22d/Model_Emergency_Call.JPG" zoom="67%"/>

Calling for help in case of emergency is modeled by a commanded behavior frame with the lift user issuing `B_EM` command. Here we define as sensible command following: `B_Em` pressed for 5 seconds = `S_B_Em`. So, when not pressed for at minimum 5 seconds, the input will be ignored by the lift controlling machine. If a `S_B_Em` (sensible emergency button pressing) is detected, the machine will issue an alarm command to the emergency number domain and sends the actual floor number of the lift and the lift ID. The Emergency number domain has stored a phone number and will call this number and also send a message with the actual floor number and the lift ID.
Here is assumed that parameters like the Lift ID and the emergency number will be assigned at installation of the lift. Another possibility would be a computer software with timetables to call which number depending which time / day. But this would need further user inputs and is not part of the basic lift problem.

#### Frame Concern:
<img src="/uploads/-/system/personal_snippet/1925960/01fd7ba2e6d3014faa04b0f2e7e1ba16/Frame_Concern_5.PNG" zoom="67%"/>

The lift #1 is stuck on floor #4 and the lift user is pressing the emergency button to call for help. The emergency number is the number of the reception staff.
1.	Lift user is issuing `B_Em` for 6 seconds 
2.	The machine is sensing the command
3.	The machine is issuing the commands `O_Alarm`, Floor 4 and Lift 1 to the emergency number domain
4.	The emergency number domain is calling the reception staff and sends a Message with “Emergency, ID = 1, Floor = 4” to the reception staff.
5.	So the reception staff gets informed about that there is a person stuck in lift number 1 at floor #4. The staff can now take further actions to help the person escaping the cabin.

### Reliability Concerns:
With effort like followed put into the reliability concerns, the first two of the steps (and partially the third) can be covered :
1.	*Detection*
2.	*Handling*
3.	*Diagnosis*

Diagnosis of the Error (step 3) is always the hardest part. For Error Nr. 5 (over-weight) we build have special hardware to show the user what is the cause of error. For the other errors we only can ride in the manual very detailed about the possible causes of the errors. This is one major part of every manual and for bigger machines it is always part of final acceptance, to train the staff to locate possible errors and fix them. Because the best machine is worthless, if having errors and nobody can fix them. For sure it would be very profitable to ride several times per week to faulty machines and charge every time the service to the customer, but that is not a good practice when wanting to have customers over long term.

#### Error detection:
The first step is to detect the errors and send stop to our safety operator:
<img src="/uploads/-/system/personal_snippet/1925960/bf74ae7b88c7584e52a214c3a3f190ff/Error_Detection.JPG" zoom="50%"/>

Our Audit machine is receiving the state from the Lift model and the input from Reset Button & Weight Sensor to detect the Errors properly and store some Information in the variable E for showing the user which error occurred. The detection is based on the following state machine.

##### Audit machine error detection state machine “Errors(L)”:
<img src="/uploads/-/system/personal_snippet/1925960/11a7ef63f698c39a038ec0add966a085/Model_Error.jpg" zoom="67%"/>

In the State machine we detect all Errors and in case of Error 1 to 4, a Stop command is issued which will force the lift to stop and switch the state machine to state 0. For going on after such an error, the reset button needs to be pressed by the lobby staff.
In case of over-weight detected by the load sensor, the lift does not need to stop, due to detecting this error only during serving a floor. If this Error occurs, the state machine will not close the door.
The next diagram shows how the safety operator will switch off the lift in case of an error and will switch the state machine for serving the floors to state 0.
The initializing after a power failure is also done by the error state machine. It is important to go in any case of restart to the next floor. Otherwise people are trapped in the lift, which is the worst we can have.

#### Error handling:
<img src="/uploads/-/system/personal_snippet/1925960/b86d3ad63344daffdf912cdde72ea9bb/Error_Handling.JPG" zoom="50%"/>

When the safety operator received the `STOP` from the Audit machine, it is necessary to let the lift properly handle these errors. This is done by sending `STOP` to the model machine, which then turns the motor immediately off and locks the lift. Also, the lift model is set to an error state.
<u>Remark:</u> not going by normal “Lift Controlling Machine” due to having here direct access to outputs.
After the safety operator issuing `OK`, the lift model starts normal operation again and the lift is intended to go on again. Therefore, it is important that also the lift model is switched to a safe state, where the operation in any case can restart without harming humans and the machine.

#### Error diagnosis:
For the overload error there is special hardware to indicate the user what he needs to do for deleting the error:
<img src="/uploads/-/system/personal_snippet/1925960/e2fa2f16c6ff478d362d10ee66617870/Error_Diagnosis.JPG" zoom="80%"/>

The alarming is done by a required behavior, if Error 5 is activated, the lift controlling machine is issuing `O_Alarm_ON` and after the Error is deactivated `O_Alarm_OFF`.

## Recombination of subproblems
Following Subproblems have been previously explained:
1.	<u>Riding to Floor(t):</u> The user pressed the request button on a floor, or the button in the cabin to ride on a specific floor
2.	<u>Show Status on Display:</u> The status of the lift gets shown on a display of every floor and a more detailed version at the reception
3.	<u>Go to Lobby after 10min stop:</u> If there was no request for 10 minutes, the lift will move to the lobby in the first floor.
4.	<u>Error Detection & Handling:</u> If there is some failure with the lift, the doors or an overweight failure, the lift is taking actions corresponding to the type of Error
5.	<u>Alarm Call:</u> if a user presses the emergency button in the lift, an alarm message is sent to a specific number which also gets called.

### Precedence:
When coming to the priority of commands, there needs to be implemented following hierarchy:
I.	Error Detection & Handling: if there is any error, the user input is not allowed to overwrite these commands
II.	Riding to Floor(t): The user commands to go from a floor to another is the core function of the whole software
III.	Go to Lobby after 10min stop: This function is just an additional feature, added due to the request of the hotel manager.

The Alarm Call is also very important, but not issuing commands to the “main” function of moving the cabin and doors. So, it could be maybe a battery installed, for enabling calls also after a power failure.


## Problem dependency graph
The problem frames are dependent on information of each other → information dependency

### Dependency without Lift Model:
Without considering the lift model, the dependency will look like following.
<img src="/uploads/-/system/personal_snippet/1925960/898d97c64ea68be6fb6c4f21d8913632/Problem_Dependency_V1.JPG" zoom="33%"/>

For example problem frame 1 and 2 just can operate without any error detected from problem frame 4.
Problem 4 is dependent on information of 1, 2 and 3 to know what the lift is doing.
Problem 2 need to show errors from 4 and requests from 1
Problem 5 is shown isolated here, due to not needing to have information from the other problems.


### Dependency with Lift Model:
When Introducing the Lift model, this dependency can be shown much different:
<img src="/uploads/-/system/personal_snippet/1925960/8553e1c87f85b07acc2461df28235580/Problem_Dependency_V2.JPG" zoom="33%"/>

This shows no dependency between the problems itself (although the Lift Model is part of 1, 2, 3, 4). But the lift model is central point of information that is shared for all problems. This leads to the possibility of splitting the problems much easier and is called architecting.
For example, problem 3 do not need to control the lift directly or know about any error. Through the lift model here can just be issued `Add_F(x)` after seeing the `serve(k)` state machine is over 10 minutes in state #1.
In real projects this has the benefit of easier splitting up big projects and just define certain interfaces between sub-problems.



## User stories



## Gherkin scenarios
